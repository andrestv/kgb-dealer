# frozen_string_literal: true

# Sorts and returns a subset of the received reviews
class SuspiciousReviewsFilter
  attr_reader :results, :reviews

  def initialize(reviews:, results: 1)
    @reviews = reviews
    @results = results
  end

  def run
    sorted_reviews_by_positivity.first(results)
  end

  private

  def sorted_reviews_by_positivity
    reviews.sort_by do |review|
      [
        -review[:score],
        -review[:word_count],
        -review[:employee_reviews].length
      ]
    end
  end
end

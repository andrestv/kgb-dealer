# kgb-dealer
A dealer for the people

> _The KGB has noticed a resurgence of overly excited reviews for a McKaig Chevrolet Buick, a dealership they have planted in the United States. In order to avoid attracting unwanted attention, you’ve been enlisted to scrape reviews for this dealership from DealerRater.com and uncover the top three worst offenders of these overly positive endorsements._

The criteria for defining an "overly positive endorsement" is, in this order:

- the score (obviously)
- the review's word count (people who write too many positive things are suspect)
- the number of employees reviewed (rating everybody at the place is a strange behavior)

So, the most suspect review will be one with the biggest score, the most words written and the most employees rated.

### Setting up and testing the project
You can setup and run this project with or without Docker.

#### With Docker
- install [Docker CE](https://docs.docker.com/install/)
- install [Docker Compose](https://docs.docker.com/compose/install/)
- clone this repository
- once inside the repo, start you container, it will download all the dependencies and build the `kgb-dealer` image:
```sh
docker-compose up -d
```
- with the container running you can run the script with:
```sh
docker-compose exec kgb-dealer ruby run.rb
```
- the script will run with the default values, if you want to know more about the available options you can see them with:
```sh
docker-compose exec kgb-dealer ruby run.rb -h
```
- to run the test suite use:
```sh
docker-compose exec kgb-dealer bin/rspec --format doc
```
- after using it, you can stop and remove the container with:
```sh
docker-compose down
```


#### Without Docker
- install [RVM](https://rvm.io/rvm/install) to help you manage the Ruby installation
- install Ruby 3.0.1 with:
```sh
rvm install 3.0.1
```
- clone this repository
- once inside the repo be sure you're using the correct version of Ruby:
```sh
rvm use 3.0.1
```
- you will also need to install the project's dependencies:
```sh
bundle install
```
- you can run the script with:
```sh
ruby run.rb
```
- the script will run with the default values, if you want to know more about the available options you can see them with:
```sh
ruby run.rb -h
```
- to run the test suite use:
```sh
bin/rspec --format doc
```

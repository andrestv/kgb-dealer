# frozen_string_literal: true

require_relative '../src/suspicious_reviews_filter'

RSpec.describe SuspiciousReviewsFilter do
  context '#run' do
    let(:reviews) do
      [
        { score: 4.8, word_count: 200, employee_reviews: %w[review1 review2 review3], title: 'Should be the third' },
        { score: 5.0, word_count: 200, employee_reviews: %w[review1 review2], title: 'Should be the first' },
        { score: 4.8, word_count: 200, employee_reviews: %w[review1 review2], title: 'Should be the fourth' },
        { score: 4.8, word_count: 240, employee_reviews: %w[review1 review2], title: 'Should be the second' }
      ]
    end

    it 'orders reviews by score, word count and number os employees reviewed' do
      result = described_class.new(reviews: reviews, results: 4).run

      expect(result[0][:title]).to eq 'Should be the first'
      expect(result[1][:title]).to eq 'Should be the second'
      expect(result[2][:title]).to eq 'Should be the third'
      expect(result[3][:title]).to eq 'Should be the fourth'
    end

    it 'returns at least one result' do
      result = described_class.new(reviews: reviews).run

      expect(result.length).to eq 1
      expect(result[0][:title]).to eq 'Should be the first'
    end
  end
end

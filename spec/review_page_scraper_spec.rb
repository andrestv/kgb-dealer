# frozen_string_literal: true

require_relative '../src/review_page_scraper'

RSpec.describe ReviewPageScraper do
  context '#run' do
    let(:url) { 'https://www.dealerrater.com/dealer/Example-Dealer-dealer-reviews-12345/page1' }
    let(:response_body) { File.read('./spec/support/review_page_example.html') }
    let(:response) { OpenStruct.new(body: response_body) }

    it 'return parsed reviews' do
      allow(HTTParty).to receive(:get).with(url, headers: described_class::HEADERS).and_return(response)

      result = described_class.new(url: url).run
      review = result[:parsed_reviews].first

      expect(result[:success?]).to be true
      expect(result[:parsed_reviews].length).to eq 1
      expect(review[:title]).to eq '"Review title"'
      expect(review[:author]).to eq 'Customer'
      expect(review[:content]).to eq 'This review has nine words. I love this place!'
      expect(review[:word_count]).to eq 9
      expect(review[:date]).to eq 'June 14, 2021'
      expect(review[:service]).to eq 'SALES VISIT - USED'
      expect(review[:score]).to eq 5.0
      expect(review[:employee_reviews].length).to eq 4
      expect(review[:employee_reviews].map { |r| r[:name] }).to eq ['Empl One', 'Empl Two', 'Empl Three', 'Empl Four']
      expect(review[:employee_reviews].map { |r| r[:score] }).to eq [5.0, 4.5, 4.8, 3.2]
    end

    it 'returns empty array when the received URL could not be reached' do
      allow(HTTParty).to receive(:get).with(url, headers: described_class::HEADERS).and_raise(StandardError, 'Message')

      result = described_class.new(url: url).run

      expect(result[:success?]).to be false
      expect(result[:parsed_reviews]).to be_empty
      expect(result[:error]).to eq 'Message'
    end
  end
end

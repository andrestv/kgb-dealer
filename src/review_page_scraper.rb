# frozen_string_literal: true

require 'httparty'
require 'nokogiri'

# Scraper class for reviews on www.dealerrater.com
class ReviewPageScraper
  attr_reader :url

  HEADERS = { 'User-Agent' => 'Httparty' }.freeze

  def initialize(url:)
    @url = url
  end

  def run
    response = HTTParty.get(url, headers: HEADERS)
    parsed_page = Nokogiri::HTML(response.body)

    { success?: true, parsed_reviews: parse_reviews(parsed_page) }
  rescue StandardError => e
    { success?: false, parsed_reviews: [], error: e.message }
  end

  private

  def parse_reviews(page)
    page.css('.review-entry').map do |entry|
      parse_entry(entry)
    end
  end

  def parse_entry(entry)
    rating = entry.css('div.review-date')
    content = entry.css('div.review-wrapper')
    employee_reviews = parse_employee_reviews(content.css('div.review-employee'))

    {
      title: content.css('h3').text.strip,
      author: content.css('span.italic').text.gsub!('- ', '').strip,
      content: content.css('p.review-content').text.strip,
      word_count: content.css('p.review-content').text.strip.split.size,
      date: rating.css('div.italic').text.strip,
      service: rating.css('div.small-text').text.strip,
      score: parse_score(rating.css('div.rating-static')),
      employee_reviews: employee_reviews
    }
  end

  def parse_employee_reviews(employees)
    employees.map do |employee|
      {
        name: employee.css('a.small-text').text.strip,
        score: parse_score(employee.css('div.rating-static'))
      }
    end
  end

  def parse_score(score)
    score.attribute('class').value[/\d(.)/].strip.to_f / 10
  end
end

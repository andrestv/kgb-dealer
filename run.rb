# frozen_string_literal: true

require 'optparse'
require_relative 'src/review_page_scraper'
require_relative 'src/suspicious_reviews_filter'

BASE_URL = 'https://www.dealerrater.com/dealer'
DEALER_REVIEW_PATH = 'McKaig-Chevrolet-Buick-A-Dealer-For-The-People-dealer-reviews-23685'

options = {
  pages: 5,
  results: 3
}

OptionParser.new do |opt|
  opt.on(
    '-p',
    '--page_num PAGENUM',
    'Number of pages to scrape (default: 5)'
  ) { |val| options[:pages] = val.to_i }
  opt.on(
    '-r',
    '--result_num RESULTNUM',
    'Number of results to show (default: 3)'
  ) { |val| options[:results] = val.to_i }
end.parse!

puts 'Scraping dealership review pages...'

reviews = options[:pages].times.each_with_object([]) do |index, output|
  print "- Page #{index + 1} of #{options[:pages]} "

  scraper = ReviewPageScraper.new(url: "#{BASE_URL}/#{DEALER_REVIEW_PATH}/page#{index + 1}")
  result = scraper.run
  output.concat(result[:parsed_reviews])

  puts result[:success?] ? 'DONE!' : result[:error]

  if result[:parsed_reviews].length < 10 && result[:success?]
    puts 'Last page reached for this dealership'
    break output
  end
end

puts "#{reviews.count} reviews found!"
puts "Filtering the top #{options[:results]} suspiciously positive review(s):"

results = SuspiciousReviewsFilter.new(reviews: reviews, results: options[:results]).run
results.each do |result|
  puts '====================================================================='
  puts "Author: #{result[:author]}"
  puts "Date: #{result[:date]}"
  puts "Score: #{result[:score]}"
  puts "Word count: #{result[:word_count]}"
  puts "Employees reviewed: #{result[:employee_reviews].length}"
  puts "Review: \"#{result[:content]}\""
end
